const baseUrl = 'http://localhost:8080';
const lunchSessionDefaultEndpoint = "/lunchSession";
const restaurantChoiceDefaultEndpoint = "/restaurantChoice";

export const environment = {
    endpoints: {
        lunchSession: {
            createSession: baseUrl + lunchSessionDefaultEndpoint,
            deleteSession: baseUrl + lunchSessionDefaultEndpoint,
            getSessionDetail: baseUrl + lunchSessionDefaultEndpoint,
            joinSession: baseUrl + lunchSessionDefaultEndpoint + "/joinSession",
        },
        restaurantChoice: {
            getRestaurantList: baseUrl + restaurantChoiceDefaultEndpoint + "/getList",
            getRestaurantChoice: baseUrl + restaurantChoiceDefaultEndpoint,
            createRestaurantChoice: baseUrl + restaurantChoiceDefaultEndpoint,
            updateRestaurantChoice: baseUrl + restaurantChoiceDefaultEndpoint,
            deleteRestaurantChoice: baseUrl + restaurantChoiceDefaultEndpoint,
        }
    },
    routes: {
        default: '',
        joinSessionScreen: 'start',
        displayScreen: 'display-screen'
    }
}