import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../env/env';


/*route guard to prevent users from directly typing in the url 
  for example, http://localhost:4200/display-screen?sessionId=4,
  Only allows navigation via router.
  if user attempts to access via url, they will be routed back to the main page (Join Session page).
  */
@Injectable({providedIn: 'root'})
export class AuthGuardComponent {
  
  constructor(private router: Router) {}
  canActivate() {
    if (this.router.navigated) {
      return true;
    }
    console.log("Attempted to access via url. Redirecting back to join session screen..")
    return this.router.navigate([environment.routes.joinSessionScreen])

  }
}
