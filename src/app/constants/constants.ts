export const LunchApplicationConstants = {
    sessionStatus: {
        ongoing: 0,
        terminated : 1
    },
    action: {
        create: 'create',
        update: 'update',
        delete: 'delete'
    },
    dialogs: {
        createSessionDialog: 'createSessionDialog',
        joinSessionDialog: 'joinSessionDialog',
        choiceActionDialog: 'choiceActionDialog'
    },
    validationErrorMessages: {
        restaurantName: {
            empty: 'Restaurant name cannot be empty',
            tooLong: 'Keep restaurant name under 20 characters'
        },
        name: {
            empty: 'Name cannot be empty',
            tooLong: 'Keep name under 20 characters'
        },
        sessionId: {
            empty: 'Session id cannot be empty',
            isNotNumber: 'Session id must be a number'
        }
    },
    successMessages: {
        session: {
            join: 'You have successfully joined the session',
            create: 'You have successfully created the session',
            refresh: 'Session has been successfully refreshed',
            terminate: 'Session has been successfully terminated'
        },
        restaurantChoice: {
            create: 'You have successfully added your restaurant choice',
            update: 'You have successfully updated your restaurant choice',
            delete: 'You have successfully deleted your restaurant choice',
            retrieveList: 'Successfully retrieved restaurant list for session',
            retrieve: 'You have successfully retrieved your restaurant choice'
        },
        navigation: {
            joinSessionScreen: 'Navigating back to main page'
        }
    }
}