import {Component, Output, EventEmitter, Inject } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatDialogTitle,
  MatDialogContent,
  MatDialogActions,
  MatDialogClose,
} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { ChoiceActionDialogChildCall, ChoiceActionDialogParameters } from '../model/dialog-interface';
import { LunchApplicationConstants } from '../constants/constants';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'choice-action-dialog',
  templateUrl: './choice-action-dialog.component.html',
  styleUrl: './choice-action-dialog.component.scss',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
    CommonModule
  ],
})
export class ChoiceActionDialog {
  @Output() submitDialogEvent: EventEmitter<ChoiceActionDialogChildCall> = new EventEmitter;

  lunchApplicationConstants = LunchApplicationConstants;

  validationErrorMessages = this.lunchApplicationConstants.validationErrorMessages;

  validationError = {
    hasError: false,
    restaurantName: {
      empty : false,
      tooLong : false
    }
  }

  constructor(
    public dialogRef: MatDialogRef<ChoiceActionDialog>,
    @Inject(MAT_DIALOG_DATA) public data: ChoiceActionDialogParameters
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onCreateSubmit() {
    this.restaurantNameValidation();
    if (!this.validationError.hasError) {
      this.submitDialogEvent.emit({ dialogName: this.lunchApplicationConstants.dialogs.choiceActionDialog, restaurantName: this.data.restaurantName, action: this.lunchApplicationConstants.action.create});
      this.dialogRef.close(); 
    }
  }

  onUpdateSubmit() {
    this.restaurantNameValidation();
    if (!this.validationError.hasError) {
      this.submitDialogEvent.emit({ dialogName: this.lunchApplicationConstants.dialogs.choiceActionDialog,restaurantName: this.data.restaurantName, action: this.lunchApplicationConstants.action.update});
      this.dialogRef.close();
    }
  }

  restaurantNameValidation() {
    this.resetValidationErrorToDefault();
    if (this.data.restaurantName === undefined || this.data.restaurantName === null || this.data.restaurantName === '') {
      this.validationError.restaurantName.empty = true
    } else if (this.data.restaurantName.length >20) {
      this.validationError.restaurantName.tooLong = true
    } else {
      this.validationError.restaurantName.empty = false;
      this.validationError.restaurantName.tooLong = false
    }

    this.validationError.hasError = this.validationError.restaurantName.empty || this.validationError.restaurantName.tooLong;
  }

  resetValidationErrorToDefault() {
    this.validationError.restaurantName.empty = false;
    this.validationError.restaurantName.tooLong = false;
    this.validationError.hasError = false;
  }


  
}
