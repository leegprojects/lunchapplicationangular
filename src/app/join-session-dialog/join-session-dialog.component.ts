import {Component, Output, EventEmitter} from '@angular/core';
import {
  MatDialogRef,
  MatDialogTitle,
  MatDialogContent,
  MatDialogActions,
  MatDialogClose,
} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { DialogChildCall } from '../model/dialog-interface';
import { LunchApplicationConstants } from '../constants/constants';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'join-session-dialog',
  templateUrl: './dialog-overview-example-dialog.html',
  styleUrl: './dialog-overview-example-dialog.scss',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
    CommonModule
  ],
})
export class JoinSessionDialog {
  @Output() submitDialogEvent: EventEmitter<DialogChildCall> = new EventEmitter;

  lunchApplicationConstants = LunchApplicationConstants;

  validationErrorMessages = this.lunchApplicationConstants.validationErrorMessages;

  validationError = {
    hasError: false,
    name: {
      empty : false,
      tooLong : false
    },
    sessionId: {
      empty : false,
      isNotNumber: false,
    }
  }


  formData = { name: '', sessionId: ''};

  constructor(
    public dialogRef: MatDialogRef<JoinSessionDialog>,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    this.validateInputs();
    console.log(this.validationError);
    if (!this.validationError.hasError) {
      this.submitDialogEvent.emit({ dialogName: LunchApplicationConstants.dialogs.joinSessionDialog, name: this.formData.name, sessionId: parseInt(this.formData.sessionId)});
      this.dialogRef.close();
    }
  }

  validateInputs() {
    this.resetValidationErrorToDefault();
    this.nameValidation();
    this.sessionIdValidation();

    this.validationError.hasError = this.validationError.name.empty || this.validationError.name.tooLong 
    || this.validationError.sessionId.empty || this.validationError.sessionId.isNotNumber;
  }

  nameValidation() {
    if (this.formData.name === undefined || this.formData.name === null || this.formData.name === '') {
      this.validationError.name.empty = true
    } else if (this.formData.name.length >20) {
      this.validationError.name.tooLong = true
    } else {
      this.validationError.name.empty = false;
      this.validationError.name.tooLong = false
    }
  }

  sessionIdValidation() {
    console.log(this.formData.sessionId)
    if (this.formData.sessionId === undefined || this.formData.sessionId === null) {
      this.validationError.sessionId.empty = true
    } else if (isNaN(parseInt(this.formData.sessionId))) {
      this.validationError.sessionId.isNotNumber = true
    } else {
      this.validationError.sessionId.empty = false;
      this.validationError.sessionId.isNotNumber = false
    }
  }

  resetValidationErrorToDefault() {
    this.validationError.name.empty = false;
    this.validationError.name.tooLong = false;
    this.validationError.sessionId.empty = false;
    this.validationError.sessionId.isNotNumber = false;
    this.validationError.hasError = false;
  }
}