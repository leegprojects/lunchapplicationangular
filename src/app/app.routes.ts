import { Routes, mapToCanActivate } from '@angular/router';
import { DisplayScreenComponent } from './display-screen/display-screen.component';
import { JoinSessionScreenComponent } from './join-session-screen/join-session-screen.component';
import { environment } from '../env/env';
import { AuthGuardComponent } from './auth-guard/auth-guard.component';

export const routes: Routes = [
    { path: environment.routes.default, redirectTo: `/${environment.routes.joinSessionScreen}`, pathMatch: 'full'},
    { path: environment.routes.joinSessionScreen, component: JoinSessionScreenComponent},
    { path: environment.routes.displayScreen, component: DisplayScreenComponent, canActivate: mapToCanActivate([AuthGuardComponent])},
];
