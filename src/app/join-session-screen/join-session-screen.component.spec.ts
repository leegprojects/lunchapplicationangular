import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinSessionScreenComponent } from './join-session-screen.component';

describe('JoinSessionScreenComponent', () => {
  let component: JoinSessionScreenComponent;
  let fixture: ComponentFixture<JoinSessionScreenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [JoinSessionScreenComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(JoinSessionScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
