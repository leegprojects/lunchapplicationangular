import { Component, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import { CreateSessionDialog } from '../create-session-dialog/create-session-dialog.component';
import { JoinSessionDialog } from '../join-session-dialog/join-session-dialog.component';
import { JoinSessionService } from './join-session.service';
import { HttpClientModule } from '@angular/common/http';
import { CreateSessionResponse, SessionDetail, UsernameRequest } from '../model/session-interface';
import { DialogChildCall } from '../model/dialog-interface';
import { LunchApplicationConstants } from '../constants/constants';
import { environment } from '../../env/env';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ErrorHandlingService } from '../message-handling-service/error-handling/error-handling-service';
import { SuccessHandlingService } from '../message-handling-service/success-handling/success-handling-service';

@Component({
  providers: [JoinSessionService],
  selector: 'app-join-session-screen',
  standalone: true,
  imports: [MatButtonModule, JoinSessionDialog, HttpClientModule, CreateSessionDialog, MatSnackBarModule],
  templateUrl: './join-session-screen.component.html',
  styleUrl: './join-session-screen.component.scss'
})
export class JoinSessionScreenComponent {
  name: string = "";
  sessionId?: number;
  childEvent = new EventEmitter<DialogChildCall>();
  lunchApplicationConstants = LunchApplicationConstants;

  constructor(private joinSessionService: JoinSessionService,
    public dialog: MatDialog,
    private router: Router,
    private errorHandlingService: ErrorHandlingService,
    private successHandlingService: SuccessHandlingService) {}

  openCreateSessionDialog(): void {
    const dialogRef = this.dialog.open(CreateSessionDialog, {
      data: {name: this.name, eventEmitter: this.childEvent},
    });

    dialogRef.componentInstance.submitDialogEvent.subscribe((data: DialogChildCall) => {
      if (data.dialogName === this.lunchApplicationConstants.dialogs.createSessionDialog) {
        this.joinSessionService.createSession(data.name).subscribe(
          (result: CreateSessionResponse) => {
            this.successHandlingService.handleSuccessMessage(this.lunchApplicationConstants.successMessages.session.create);
            this.router.navigate([`/${environment.routes.displayScreen}`], { queryParams: { sessionId: result.sessionId }})
          },
          (error) => {
            this.errorHandlingService.handleError(error);
          }
        );
      }
    });
  }

  openJoinSessionDialog(): void {
    const dialogRef = this.dialog.open(JoinSessionDialog, {
      data: {name: this.name, eventEmitter: this.childEvent},
    });

    dialogRef.componentInstance.submitDialogEvent.subscribe((data: DialogChildCall) => {
      if (data.dialogName === this.lunchApplicationConstants.dialogs.joinSessionDialog) {
        const joinSessionRequest: UsernameRequest = {
          username: data.name
        }
        this.joinSessionService.joinSession(joinSessionRequest, data.sessionId!).subscribe(
          (result: SessionDetail) => {
            this.successHandlingService.handleSuccessMessage(this.lunchApplicationConstants.successMessages.session.join);
            this.router.navigate([`/${environment.routes.displayScreen}`], { queryParams: { sessionId: data.sessionId }})
          },
          (error) => {
            this.errorHandlingService.handleError(error);
          }
        );
      }
    });
  }
}

