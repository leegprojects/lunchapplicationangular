import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../env/env';
import { Observable } from 'rxjs';
import { CreateSessionResponse, SessionDetail, UsernameRequest } from '../model/session-interface';

@Injectable({
  providedIn: 'root'
})
export class JoinSessionService {

  constructor(
    private http: HttpClient
  ) { }

  createSession(username: string): Observable<CreateSessionResponse> {
    const createSessionUrl: string = `${environment.endpoints.lunchSession.createSession}`
    return this.http.post<CreateSessionResponse>(createSessionUrl, {"username" : username}, { withCredentials: true });//withCredentials needed to set cookies because of cors
  }

  joinSession(joinSessionRequest: UsernameRequest, sessionId: number): Observable<SessionDetail> {
    const joinSessionUrl: string = `${environment.endpoints.lunchSession.joinSession}/${sessionId}?username=${joinSessionRequest.username}`
    return this.http.get<SessionDetail>(joinSessionUrl, { withCredentials: true });//withCredentials needed to set cookies because of cors
  }
}
