export interface SessionDetail {
    sessionId : number,
    createdBy: string,
    sessionUuid: string,
    createdAt: string,
    sessionStatus: number
}

export interface CreateSessionResponse {
    sessionId: number,
    sessionUuid: string,
}

export interface UsernameRequest {
    username: string
}
  