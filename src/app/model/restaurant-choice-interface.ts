export interface RestaurantChoice {
    id: number,
    createdBy: string,
    restaurantName: string,
    sessionId: number
}

export interface RestaurantChoiceRequest {
    restaurantName: string,
    username: string,
}