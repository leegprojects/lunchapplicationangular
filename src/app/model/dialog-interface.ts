export interface DialogChildCall {
    dialogName: string;
    sessionId?: number;
    name: string;
}

export interface ChoiceActionDialogChildCall {
    dialogName: string;
    restaurantName: string;
    action: string;
}

export interface ChoiceActionDialogParameters {
    restaurantName: string;
    action: string;
}