import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayScreenTable } from './display-screen-table.component';

describe('DisplayScreenTableComponent', () => {
  let component: DisplayScreenTable;
  let fixture: ComponentFixture<DisplayScreenTable>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DisplayScreenTable]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DisplayScreenTable);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
