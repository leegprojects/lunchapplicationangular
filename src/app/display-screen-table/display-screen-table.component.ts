import {Component, Input} from '@angular/core';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'display-screen-table',
  styleUrls: ['display-screen-table.component.scss'],
  templateUrl: 'display-screen-table.component.html',
  standalone: true,
  imports: [MatTableModule, CommonModule],
})
export class DisplayScreenTable  {
  @Input() dataSource = new MatTableDataSource<any>();
  @Input() displayedColumns: string[] = ['restaurantChoiceName', 'createdBy'];
}
