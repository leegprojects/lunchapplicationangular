import {Component, Output, EventEmitter} from '@angular/core';
import {
  MatDialogRef,
  MatDialogTitle,
  MatDialogContent,
  MatDialogActions,
  MatDialogClose,
} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { DialogChildCall } from '../model/dialog-interface';
import { LunchApplicationConstants } from '../constants/constants';
import { CommonModule } from '@angular/common';

export interface DialogData {
  name: string;
  eventEmitter: EventEmitter<DialogChildCall>;
}

@Component({
  selector: 'create-session-dialog',
  templateUrl: './dialog-overview-example-dialog.html',
  styleUrl: './dialog-overview-example-dialog.scss',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
    CommonModule
  ],
})
export class CreateSessionDialog {
  @Output() submitDialogEvent: EventEmitter<{}> = new EventEmitter;

  lunchApplicationConstants = LunchApplicationConstants;

  validationErrorMessages = this.lunchApplicationConstants.validationErrorMessages;

  validationError = {
    hasError: false,
    name: {
      empty : false,
      tooLong : false
    }
  }

  formData = { name: ''};

  constructor(
    public dialogRef: MatDialogRef<CreateSessionDialog>,
    // @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    this.nameValidation();
    console.log(this.validationError);
    if (!this.validationError.hasError) {
      console.log("triggered");
      this.submitDialogEvent.emit({ dialogName: LunchApplicationConstants.dialogs.createSessionDialog, name: this.formData.name});
      this.dialogRef.close();
    }
    console.log("not triggered");
  }

  nameValidation() {
    this.resetValidationErrorToDefault();
    if (this.formData.name === undefined || this.formData.name === null || this.formData.name === '') {
      this.validationError.name.empty = true
    } else if (this.formData.name.length >20) {
      this.validationError.name.tooLong = true
    } else {
      this.validationError.name.empty = false;
      this.validationError.name.tooLong = false
    }

    this.validationError.hasError = this.validationError.name.empty || this.validationError.name.tooLong;
  }

  resetValidationErrorToDefault() {
    this.validationError.name.empty = false;
    this.validationError.name.tooLong = false;
    this.validationError.hasError = false;
  }
}