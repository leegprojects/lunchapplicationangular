import { TestBed } from '@angular/core/testing';

import { DisplayScreenService } from './display-screen.service';

describe('DisplayScreenService', () => {
  let service: DisplayScreenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DisplayScreenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
