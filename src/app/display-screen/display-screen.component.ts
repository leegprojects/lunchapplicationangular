import { Component, EventEmitter } from '@angular/core';
import { DisplayScreenTable } from '../display-screen-table/display-screen-table.component';
import { DisplayScreenService } from './display-screen.service';
import { ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { environment } from '../../env/env';
import { CommonModule } from '@angular/common';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule} from '@angular/forms';
import {MatDividerModule} from '@angular/material/divider';
import { RestaurantChoiceRequest, RestaurantChoice } from '../model/restaurant-choice-interface';
import { SessionDetail } from '../model/session-interface';
import { LunchApplicationConstants } from '../constants/constants';
import { ChoiceActionDialog } from '../choice-action-dialog/choice-action-dialog.component';
import { ChoiceActionDialogChildCall } from '../model/dialog-interface';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ErrorHandlingService } from '../message-handling-service/error-handling/error-handling-service';
import { SuccessHandlingService } from '../message-handling-service/success-handling/success-handling-service';

@Component({
  providers: [DisplayScreenService],
  selector: 'app-display-screen',
  standalone: true,
  imports: [MatSnackBarModule, MatDividerModule, MatInputModule, MatFormFieldModule, FormsModule, CommonModule, MatButtonModule, DisplayScreenTable],
  templateUrl: './display-screen.component.html',
  styleUrl: './display-screen.component.scss'
})
export class DisplayScreenComponent {
  lunchApplicationConstants = LunchApplicationConstants;
  sessionDetail: SessionDetail = {
    sessionId: -1,
    createdBy: '',
    sessionUuid: '',
    createdAt: '',
    sessionStatus: -1
  };

  defaultRestaurantChoice : RestaurantChoice = {
    id: -1,
    createdBy: '',
    restaurantName: '',
    sessionId: -1
  };

  restaurantChoice?: RestaurantChoice = this.defaultRestaurantChoice;
  username: string = "";
  isOwner: boolean = false;
  dataSource = new MatTableDataSource<any>();
  displayedColumns: string[] = ['restaurantChoiceName', 'createdBy']; // Add more column names as needed
  childEvent = new EventEmitter<ChoiceActionDialogChildCall>(); //to receive data from choiceActionDialog
  

  constructor(private displayScreenService: DisplayScreenService,
    public dialog: MatDialog,
    private route: ActivatedRoute, 
    private cookieService: CookieService,
    private router: Router, 
    private errorHandlingService: ErrorHandlingService,
    private successHandlingService: SuccessHandlingService) {}

  ngOnInit() {
    this.sessionDetail.sessionId = +this.route.snapshot.queryParamMap.get('sessionId')!;
    this.getSessionCall();
  }

  // pollRestaurantListData(): void {
  //   this.displayScreenService.getRestaurantList(this.sessionId).subscribe(
  //     (result: RestaurantChoice[]) => {
  //       this.setDataSource(result);
  //       if (this.sessionDetail && this.sessionDetail.sessionStatus === this.lunchApplicationConstants.sessionStatus.ongoing) {
  //         console.log("abc");
  //         setTimeout(() => this.pollRestaurantListData(), 5000); // Poll every 5 seconds (adjust as needed)
  //       }
  //     },
  //     (error) => {
  //       console.error('Error with getRestaurantList Endpoint', error);
  //     }
  //   ); 
  // }

  getSessionCall() {
    this.displayScreenService.getSessionDetails(this.sessionDetail.sessionId).subscribe(
      (sessionDetail: SessionDetail) => {
        this.sessionDetail = sessionDetail;
        this.validateSession(sessionDetail);
        this.getRestaurantListCall();
        this.getRestaurantChoiceCall();
      },
      (error) => {
        this.errorHandlingService.handleError(error);
        this.returnToJoinSessionScreen();
      }
    );
  }

  refreshSessionAndRestaurantListCall() {
    this.displayScreenService.getSessionDetails(this.sessionDetail.sessionId).subscribe(
      (sessionDetail: SessionDetail) => {
        this.sessionDetail = sessionDetail;
        this.getRestaurantListCall();
        this.successHandlingService.handleSuccessMessage(this.lunchApplicationConstants.successMessages.session.refresh);
            
      },
      (error) => {
        this.errorHandlingService.handleError(error);
        this.returnToJoinSessionScreen();
      }
    );
  }

  getRestaurantListCall() {
    this.displayScreenService.getRestaurantList(this.sessionDetail.sessionId).subscribe(
      (result: RestaurantChoice[]) => {
        this.setDataSource(result);
        // this.successHandlingService.handleSuccessMessage(this.lunchApplicationConstants.successMessages.restaurantChoice.retrieveList);
      },
      (error) => {
        // this.errorHandlingService.handleError(error);
      }
    );
  }

  createRestaurantChoiceCall(createRestaurantChoiceRequest: RestaurantChoiceRequest) {
    this.displayScreenService.createRestaurantChoice(createRestaurantChoiceRequest, this.sessionDetail.sessionId).subscribe(
      (result: RestaurantChoice) => {
        this.restaurantChoice = result; //sets response data into restaurantChoice
        this.getRestaurantListCall();
        this.successHandlingService.handleSuccessMessage(this.lunchApplicationConstants.successMessages.restaurantChoice.create);
      },
      (error) => {
        this.errorHandlingService.handleError(error);
      }
    );
  }

  terminateSessionCall() {
    this.displayScreenService.terminateSession(this.sessionDetail.sessionId).subscribe(
      (sessionDetail: SessionDetail) => {
        this.sessionDetail = sessionDetail;
        this.getRestaurantListCall();
        this.successHandlingService.handleSuccessMessage(this.lunchApplicationConstants.successMessages.session.terminate);
      },
      (error) => {
        this.errorHandlingService.handleError(error);
      }
    );
  }

  getRestaurantChoiceCall() {
    this.displayScreenService.getRestaurantChoice(this.sessionDetail.sessionId, this.username).subscribe(
      (result: RestaurantChoice) => {
        this.restaurantChoice = result;
        // this.successHandlingService.handleSuccessMessage(this.lunchApplicationConstants.successMessages.restaurantChoice.retrieve);
      
      },
      (error) => {
        // this.errorHandlingService.handleError(error);
      }
    );
  }

  updateRestaurantChoiceCall(updateRestaurantChoiceRequest: RestaurantChoiceRequest) {
    this.displayScreenService.updateRestaurantChoice(updateRestaurantChoiceRequest, this.sessionDetail.sessionId, this.restaurantChoice!.id).subscribe(
      (result: RestaurantChoice) => {
        this.restaurantChoice = result; //sets response data into restaurantChoice
        this.getRestaurantListCall();
        this.successHandlingService.handleSuccessMessage(this.lunchApplicationConstants.successMessages.restaurantChoice.update);
      },
      (error) => {
        this.errorHandlingService.handleError(error);
      }
    );
  }

  deleteRestaurantChoiceCall() {
    this.displayScreenService.deleteRestaurantChoice(this.sessionDetail.sessionId, this.restaurantChoice!.id).subscribe(
      (result: RestaurantChoice) => {
        this.restaurantChoice = this.defaultRestaurantChoice; //resets restaurantChoice back to default. Make an api call to fetch back restaurant choice details?
        this.getRestaurantListCall();
        this.successHandlingService.handleSuccessMessage(this.lunchApplicationConstants.successMessages.restaurantChoice.delete);
      },
      (error) => {
        this.errorHandlingService.handleError(error);
      }
    );
  }

  validateSession(sessionDetail: SessionDetail) {
    if (!sessionDetail) {
      this.returnToJoinSessionScreen();
    } else if (sessionDetail.sessionStatus === LunchApplicationConstants.sessionStatus.terminated) {//prevents user from joining a session that has already ended
      this.deleteSessionCookie(sessionDetail.sessionUuid);//remove cookies of a session that has already been terminated
      this.returnToJoinSessionScreen();//routes user back to the main page
    } else {
      this.validateUser(sessionDetail);
    }
  }

  validateUser(sessionDetail: SessionDetail) {
    //cookieValue = [owner/user]_[username]+[username(if there are spaces in username)]
    //eg. owner_james+tan+jia+hao, user_mary+yang+mei+le
    const cookieValue: string = this.getSessionCookie(sessionDetail.sessionUuid);
    if (cookieValue) {
      const result = cookieValue.split('_');
      result[0] === "owner" ? this.isOwner = true : this.isOwner = false; // contains either owner or user
      this.username = result[1].replace(/\+/g, ' '); //sets username from spilt string and replaces all '+' with ' '
    }
    if (!this.username) {
      /*
      will evaluate to true if value is not:
        null
        undefined
        NaN
        empty string ''
        0
        false
      */
      this.returnToJoinSessionScreen();
    }
  }

  getSessionCookie(sessionUuid: string): string {
    return this.cookieService.get(sessionUuid);
  }

  deleteSessionCookie(sessionUuid: string): void {
    this.cookieService.delete(sessionUuid);
  }

  setDataSource(restaurantListData: RestaurantChoice[]) {
    // const data = [
    //   { restaurantChoiceName: "xyz grillbar", createdBy: 'Mary Lim' },
    //   { restaurantChoiceName: "123 seafood house", createdBy: 'James Tan' },
    //   // Add more data as needed
    // ];
    this.dataSource.data = restaurantListData;
  }

  addChoice() {
    const createRestaurantChoiceRequest :RestaurantChoiceRequest = {
      restaurantName: this.restaurantChoice!.restaurantName,
      username: this.username,
    }
    this.createRestaurantChoiceCall(createRestaurantChoiceRequest);
  }

  terminateSession() {
    this.terminateSessionCall();
  }

  returnToJoinSessionScreen() {
    this.successHandlingService.handleSuccessMessage(this.lunchApplicationConstants.successMessages.navigation.joinSessionScreen);
    this.router.navigate([environment.routes.joinSessionScreen])
  }

  openCreateRestaurantChoiceDialog(): void {
    console.log("create restaurant choice button clicked");
    const dialogRef = this.dialog.open(ChoiceActionDialog, {
      data: {name: "", eventEmitter: this.childEvent, action: this.lunchApplicationConstants.action.create},
    });

    dialogRef.componentInstance.submitDialogEvent.subscribe((data: ChoiceActionDialogChildCall) => {
      if (data.dialogName === LunchApplicationConstants.dialogs.choiceActionDialog && data.action === LunchApplicationConstants.action.create) {
        this.restaurantChoice!.restaurantName = data.restaurantName //sets restaurant name from dialog into the restaurantChoice
        const createRestaurantChoiceRequest: RestaurantChoiceRequest = {
          restaurantName: this.restaurantChoice!.restaurantName,
          username: this.username,
        } 
        this.createRestaurantChoiceCall(createRestaurantChoiceRequest);
      }
    });
  }

  openUpdateRestaurantChoiceDialog(): void {
    const dialogRef = this.dialog.open(ChoiceActionDialog, {
      data: {name: this.restaurantChoice!.restaurantName, eventEmitter: this.childEvent, action: this.lunchApplicationConstants.action.update},
    });

    dialogRef.componentInstance.submitDialogEvent.subscribe((data: ChoiceActionDialogChildCall) => {
      if (data.dialogName === LunchApplicationConstants.dialogs.choiceActionDialog && data.action === LunchApplicationConstants.action.update) {
        this.restaurantChoice!.restaurantName = data.restaurantName //sets restaurant name from dialog into the restaurantChoice
        const updateRestaurantChoiceRequest: RestaurantChoiceRequest = {
          restaurantName: this.restaurantChoice!.restaurantName,
          username: this.username,
        } 
        this.updateRestaurantChoiceCall(updateRestaurantChoiceRequest);
      }
    });
  }

  
}
