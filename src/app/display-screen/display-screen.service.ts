import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../env/env';
import { Observable } from 'rxjs';
import { RestaurantChoiceRequest, RestaurantChoice } from '../model/restaurant-choice-interface';
import { SessionDetail } from '../model/session-interface';

@Injectable({
  providedIn: 'root'
})
export class DisplayScreenService {

  constructor(
    private http: HttpClient
  ) { }

  getSessionDetails(sessionId: number): Observable<SessionDetail> {
    const getSessionDetailUrl: string = `${environment.endpoints.lunchSession.getSessionDetail}/${sessionId}`
    return this.http.get<SessionDetail>(getSessionDetailUrl, { withCredentials: true });//withCredentials needed to set cookies because of cors
  }

  getRestaurantList(sessionId: number): Observable<RestaurantChoice[]> {
    const getRestaurantListUrl: string = `${environment.endpoints.restaurantChoice.getRestaurantList}/${sessionId}`
    return this.http.get<RestaurantChoice[]>(getRestaurantListUrl, { withCredentials: true });//withCredentials needed to set/send cookies because of cors
  }

  createRestaurantChoice(createRestaurantChoice: RestaurantChoiceRequest, sessionId: number): Observable<RestaurantChoice> {
    const createRestaurantChoiceUrl: string = `${environment.endpoints.restaurantChoice.createRestaurantChoice}/${sessionId}`
    return this.http.post<RestaurantChoice>(createRestaurantChoiceUrl, createRestaurantChoice,{ withCredentials: true });//withCredentials needed to set/send cookies because of cors
  }

  terminateSession(sessionId: number) : Observable<SessionDetail>{
    const terminateSessionUrl: string = `${environment.endpoints.lunchSession.deleteSession}/${sessionId}`
    return this.http.delete<SessionDetail>(terminateSessionUrl, { withCredentials: true });//withCredentials needed to set cookies because of cors
  }

  getRestaurantChoice(sessionId: number, username: string): Observable<RestaurantChoice> {
    const getRestaurantChoiceUrl: string = `${environment.endpoints.restaurantChoice.getRestaurantChoice}/${sessionId}?username=${username}`
    return this.http.get<RestaurantChoice>(getRestaurantChoiceUrl, { withCredentials: true });//withCredentials needed to set/send cookies because of cors
  }

  updateRestaurantChoice(updateRestaurantChoice: RestaurantChoiceRequest, sessionId: number, restaurantChoiceId: number): Observable<RestaurantChoice> {
    const updateRestaurantChoiceUrl: string = `${environment.endpoints.restaurantChoice.updateRestaurantChoice}/${sessionId}/${restaurantChoiceId}`
    return this.http.patch<RestaurantChoice>(updateRestaurantChoiceUrl, updateRestaurantChoice,{ withCredentials: true });//withCredentials needed to set/send cookies because of cors
  }

  deleteRestaurantChoice(sessionId: number, restaurantChoiceId: number): Observable<RestaurantChoice> {
    const deleteRestaurantChoiceUrl: string = `${environment.endpoints.restaurantChoice.deleteRestaurantChoice}/${sessionId}/${restaurantChoiceId}`
    return this.http.delete<RestaurantChoice>(deleteRestaurantChoiceUrl, { withCredentials: true });//withCredentials needed to set/send cookies because of cors
  }
}
