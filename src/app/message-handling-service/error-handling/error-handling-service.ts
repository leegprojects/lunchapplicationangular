import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlingService {

    constructor(private snackBar: MatSnackBar) 
    {}

    handleError(httpErrorResponse: HttpErrorResponse): void {
        const snackBarConfig: MatSnackBarConfig = {
          duration: 5000, // Duration in milliseconds
          horizontalPosition: 'end', //align snackbar to right
          verticalPosition: 'top', // align snackbar to top
        };

        this.snackBar.open(httpErrorResponse.error.errorDetails, 'Close', snackBarConfig);
    }

    

}
