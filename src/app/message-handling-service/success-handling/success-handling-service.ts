import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SuccessHandlingService {

    constructor(private snackBar: MatSnackBar) 
    {}

    handleSuccessMessage(successMessage: string): void {
      const snackBarConfig: MatSnackBarConfig = {
        duration: 5000, // Duration in milliseconds
        horizontalPosition: 'end', //align snackbar to left
        verticalPosition: 'top', // align snackbar to top
      };

      this.snackBar.open(successMessage, 'Close', snackBarConfig);
  }

}
